<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDmsDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dms_documents', function (Blueprint $table) {
            $table->increments('id');
            $table->string('number')->nullable();
            $table->string('ids')->nullable();
            $table->timestamp('date')->nullable();
            $table->timestamp('created')->nullable();
            $table->string('from')->nullable();
            $table->string('to')->nullable();
            $table->string('about')->nullable();
            $table->string('summary')->nullable();
            $table->string('label')->nullable();
            $table->softDeletes();
            $table->timestamps();
            $table->unique('number');
            $table->unique('ids');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dms_documents');
    }
}
