<?php
/**
 * Created by PhpStorm.
 * User: andika
 * Date: 05/11/2017
 * Time: 23:06
 */

namespace App\DataTables\Dms;

use App\Models\Dms\Profile;
use App\Models\Dms\User;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Services\DataTable;

class UserDataTable extends DataTable
{
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);
        $dataTable->addColumn('action', 'dms.users.datatables_actions');
        $dataTable->addColumn('full_name', function(User $user){
            if ($user->profile) {
                return $user->profile->full_name;
            } else
                return '';
        });
        $dataTable->addColumn('employee_id', function(User $user){
            if ($user->profile) {
                return $user->profile->employee_id;
            } else
                return '';
        });
        $dataTable->addColumn('position', function(User $user){
            if ($user->profile) {
                return $user->profile->position;
            } else
                return '';
        });
        $dataTable->addColumn('division', function(User $user){
            if ($user->profile) {
                return $user->profile->division;
            } else
                return '';
        });
        $dataTable->addColumn('department', function(User $user){
            if ($user->profile) {
                return $user->profile->department;
            } else
                return '';
        });
        $dataTable->addColumn('location', function(User $user){
            if ($user->profile) {
                return $user->profile->location;
            } else
                return '';
        });
        return $dataTable;
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Dms\Document $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(User $model)
    {
        $newQuery = $model->with('profile')->newQuery();
        return $newQuery;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '80px'])
            ->parameters([
                'dom'     => 'Bfrtip',
                'order'   => [[0, 'desc']],
                'buttons' => [
                    'create',
                    'export',
                    'print',
                    'reset',
                    'reload',
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'email',
            ['data'=>'full_name','title'=>'Nama Pegawai'],
            ['data'=>'employee_id','title'=>'NIK'],
            ['data'=>'division','title'=>'Divisi'],
            ['data'=>'department','title'=>'Departemen'],
            ['data'=>'position','title'=>'Posisi'],
            ['data'=>'location','title'=>'Lokasi']
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'usersdatatable_' . time();
    }
}