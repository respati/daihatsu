<?php

namespace App\Models\Dms;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Profile extends Model
{
    public $table = 'user_profiles';
    public $fillable = [
        'user_id',
        'f_name',
        'l_name',
        'employee_id',
        'department',
        'division',
        'position',
        'location'
    ];

    protected $casts = [
        'user_id' => 'integer',
        'f_name' => 'string',
        'l_name' => 'string',
        'employee_id' => 'string',
        'department' => 'string',
        'division' => 'string',
        'position' => 'string',
        'location' => 'string'
    ];

    protected $appends = [
        'full_name'
    ];

    public static $rules = [
    ];

    public function user() {
        return $this->belongsTo(User::class)->withDefault();
    }

    public function getFullNameAttribute(){
        return ucfirst($this->f_name).' '.ucfirst($this->l_name);
    }
}
