<?php

namespace App\Models\Dms;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Document
 * @package App\Models
 * @version November 5, 2017, 8:27 am UTC
 *
 * @property string number
 * @property string ids
 * @property string|\Carbon\Carbon date
 * @property string|\Carbon\Carbon created
 * @property string from
 * @property string to
 * @property string about
 * @property string summary
 * @property string label
 */
class Document extends Model
{
    public $table = 'dms_documents';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $dates = ['date','created'];


    public $fillable = [
        'number',
        'ids',
        'date',
        'created',
        'from',
        'to',
        'about',
        'summary',
        'label'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'number' => 'string',
        'ids' => 'string',
        'from' => 'string',
        'to' => 'string',
        'about' => 'string',
        'summary' => 'string',
        'label' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'number' => 'sometimes|unique:dms_documents',
        'ids' => 'sometimes|unique:dms_documents'
    ];

    
}
