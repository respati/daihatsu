<?php

namespace App\Http\Controllers\Dms;

use App\DataTables\Dms\UserDataTable;
use App\Repositories\Dms\UserRepository;
use Illuminate\Http\Request;

class UserController extends AppBaseController
{
    private $userRepository;

    public function __construct(UserRepository $repo)
    {
        $this->userRepository= $repo;
    }

    public function index(UserDataTable $userDataTable)
    {
        return $userDataTable->render('dms.users.index');
    }
}
