<!-- Number Field -->
<div class="col-md-6">
    <div class="form-group col-sm-12">
        {!! Form::label('kind', 'Jenis Surat:') !!}
        {!! Form::select('kind', ['I'=>'Surat Masuk','O'=>'Surat Keluar'], null, ['class' => 'form-control','required']) !!}
    </div>

    <div class="form-group col-sm-12">
        {!! Form::label('number', 'Nomor Surat:') !!}
        {!! Form::text('number', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Ids Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('ids', 'ID Surat:') !!}
        {!! Form::text('ids', null, ['class' => 'form-control']) !!}
    </div>

    <!-- About Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('about', 'Perihal:') !!}
        {!! Form::text('about', null, ['class' => 'form-control']) !!}
    </div>

    <!-- From Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('from', 'Dari:') !!}
        {!! Form::text('from', null, ['class' => 'form-control']) !!}
    </div>

    <!-- To Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('to', 'Kepada:') !!}
        {!! Form::text('to', null, ['class' => 'form-control']) !!}
    </div>

</div>

<div class="col-md-6">

    <!-- Date Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('date', 'Date:') !!}
        {!! Form::date('date', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Created Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('created', 'Created:') !!}
        {!! Form::date('created', null, ['class' => 'form-control']) !!}
    </div>
    <!-- Label Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('label', 'Label:') !!}
        {!! Form::text('label', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Summary Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('summary', 'Summary:') !!}
        {!! Form::textarea ('summary', null, ['class' => 'form-control']) !!}
    </div>
</div>
<div class="col-sm-12">

    <!-- Summary Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('files[]', 'Upload Files:') !!}
        {!! Form::file ('files[]', ['id'=>'filesupload',
                                    'class' => 'form-control',
                                    'multiple'=>'multiple',
                                    'data-preview-file-type'=>'csv'])
                                    !!}
    </div>

    <!-- Submit Field -->
    <div class="form-group col-sm-12">
        {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
        <a href="{!! route('documents.index') !!}" class="btn btn-default">Cancel</a>
    </div>
</div>


@push('styles')
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.5/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />

@endpush

@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.5/js/plugins/piexif.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.5/js/plugins/sortable.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.5/js/plugins/purify.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
    {{--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" type="text/javascript"></script>--}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.5/js/fileinput.min.js"></script>
    {{--<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.5/js/locales/LANG.js"></script>--}}

<script>
    $(function() {
        var opt = {'showUpload':false, 'showCancel':false, 'showClose':false, 'hiddenThumbnailContent':true,
        'allowedFileExtensions':['jpg','png','txt','docx','doc','pdf','zip','7zip','rar'],
            previewFileIconSettings: {
                'doc': '<i class="fa fa-file-word-o text-primary"></i>',
                'xls': '<i class="fa fa-file-excel-o text-success"></i>',
                'ppt': '<i class="fa fa-file-powerpoint-o text-danger"></i>',
                'jpg': '<i class="fa fa-file-photo-o text-warning"></i>',
                'pdf': '<i class="fa fa-file-pdf-o text-danger"></i>',
                'zip': '<i class="fa fa-file-archive-o text-muted"></i>',
            },
        'removeFromPreviewOnError':true};
        $("#filesupload").fileinput(opt);
        console.log( "ready!" );
    });
</script>
@endpush