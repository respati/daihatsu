@section('css')
    @include('dms.layouts.datatables_css')
@endsection

{!! $dataTable->table(['width' => '100%']) !!}

@section('scripts')
    @include('dms.layouts.datatables_js')
    {!! $dataTable->scripts() !!}
@endsection