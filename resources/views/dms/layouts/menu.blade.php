<li class="">
    <a href="#"><span>Beranda</span></a>
</li>

<li class="{{ Request::is('users*') ? 'active' : '' }}">
    <a href="{!! route('users.index') !!}"><i class="fa fa-edit"></i><span>Anggota</span></a>
</li>

<li class="">
    <a href="#"><span>Klasifikasi Dokumen</span></a>
</li>

<li class="{{ Request::is('document.create') ? 'active' : '' }}">
    <a href="{!! route('documents.create') !!}"><i class="fa fa-edit"></i><span>Entri Surat</span></a>
</li>

<li class="{{ Request::is('documents*') ? 'active' : '' }}">
    <a href="{!! route('documents.index') !!}"><i class="fa fa-edit"></i><span>Pencarian</span></a>
</li>

<li class="">
    <a href="#"><span>Penyimpanan</span></a>
</li>

<li class="">
    <a href="#"><span>Peminjaman</span></a>
</li>

<li class="">
    <a href="#"><span>Pemusnahan</span></a>
</li>

<li class="">
    <a href="#"><span>Laporan</span></a>
</li>

<li class="">
    <a href=""><span>Pengaturan</span></a>
</li>

{{--<li class="{{ Request::is('documents*') ? 'active' : '' }}">--}}
    {{--<a href="{!! route('documents.index') !!}"><i class="fa fa-edit"></i><span>Keluar</span></a>--}}
{{--</li>--}}

