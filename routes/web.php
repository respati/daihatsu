<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::domain('dms.'.env('APP_BASE', false))->group(function () {

    Route::get('/', function () {
        return view('dms.welcome');
    });
    Route::get('/document_entry/', function () {
        return view('dms.pages.document_entry');
    })->name('documents.entry');

    Auth::routes();

    Route::get('/home', 'Dms\HomeController@index');

    Route::resource('documents', 'Dms\DocumentController');
    Route::resource('users', 'Dms\UserController');

});

Route::domain(env('APP_BASE', false))->group(function () {
    Route::get('/', function () {
        return view('welcome');
    });
    Auth::routes();
    Route::get('/home', 'HomeController@index')->name('home');
});


